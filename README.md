*a note by la-ninpre*

> this is my fork of dwm. it is pretty much close with the original, but
> it will evolve as i learn and as i need more functionality.
>
> patches applied:
> 
> * [dwm-autostart][1]

[1]:https://dwm.suckless.org/patches/autostart/

# dwm -- dynamic window manager

dwm is an extremely fast, small, and dynamic window manager for x.

## requirements

in order to build dwm you need the xlib header files.

used fonts:

* lato
* jetbrains mono

## installation

edit `config.mk` to match your local setup (dwm is installed into the
`/usr/local` namespace by default).

afterwards enter the following command to build and install dwm
(if necessary as root):

```terminal
# make clean install
```

## running dwm

add the following line to your `.xinitrc` to start dwm using startx:

```
exec dwm
```

in order to connect dwm to a specific display, make sure that the `DISPLAY`
environment variable is set correctly, e.g.:

```
DISPLAY=foo.bar:1 exec dwm
```

(this will start dwm on display :1 of the host foo.bar.)

in order to display status info in the bar, you can do something like this
in your `.xinitrc`:

```
while xsetroot -name "`date` `uptime | sed 's/.*,//'`"
do
    sleep 1
done &
exec dwm
```

## configuration

the configuration of dwm is done by creating a custom config.h and (re)compiling
the source code.
